import * as sagas from '../ProfileSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as profileActions from '../ProfileActions';
import * as signInActions from '../../signIn/SignInActions';
import * as notificationActions from '../../components/notification/NotificationActions';
import { replace } from 'react-router-redux';
import Moment from 'moment';
import { DATE_FORMAT_FOR_SIGN_IN } from '../../services/Constants';

const localStorageMock = {
  getItem: jest.fn(),
  removeItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
  userID: '1',
  access_token: '123456',
};

global.localStorage = localStorageMock;

describe('Get user profile data', () => {

  it('Should successfully get all profile data', () => {
    const generator = sagas.handleGetUserProfile();

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'clients'));

    next = generator.next({ data: {} });
    expect(next.value).toEqual(put(profileActions.getUserProfileSuccess({})));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleGetUserProfile();

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'clients'));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put(notificationActions.createNotification('Can\'t get user profile data!', true)));

    next = generator.next();
    expect(next.value).toEqual(put(profileActions.getUserProfileError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

