import { userProfileActionTypes } from './ProfileContants';

const initialState = {
  userData: {
    email: 'jurahalas@gmail.com',
    first_name: 'Yuriy',
    last_name: 'Halas',
  },
};

export default function profileReducer(state = initialState, action) {
  const { type, payload = {} } = action;
  const { error } = payload;
  const errorMessage = (error && error.message) || '';

  switch(type) {

    case userProfileActionTypes.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case userProfileActionTypes.GET_USER_PROFILE_SUCCESS:
      return {
        ...state,
        userData: {
          ...action.payload.data,
        },
        loading: false,
      };

    case userProfileActionTypes.GET_USER_PROFILE_ERROR:
      return {
        ...state,
        error: errorMessage,
        loading: false,
      };

    default:
      return state;
  }
}