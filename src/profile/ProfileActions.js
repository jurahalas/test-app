import { userProfileActionTypes } from './ProfileContants';

export function getUserProfileRequest() {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_REQUEST,
  };
}

export function getUserProfileSuccess(data) {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getUserProfileError(error) {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_ERROR,
    payload: {
      error,
    },
  };
}