import { takeEvery, put, call, all } from 'redux-saga/effects';
import { processRequest } from '../services/Api';
import { userProfileActionTypes } from '../profile/ProfileContants';
import * as profileActions from '../profile/ProfileActions';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeEvery(userProfileActionTypes.GET_USER_PROFILE_REQUEST, handleGetUserProfile),
  ]);
}

export function* handleGetUserProfile() {
  try {
    const { data } = yield call(processRequest, 'clients');

    yield put(profileActions.getUserProfileSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Can\'t get user profile data!', true));
    yield put(profileActions.getUserProfileError(e));
  }
}