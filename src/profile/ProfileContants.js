import keyMirror from 'keymirror';

export const userProfileActionTypes = keyMirror(
  {
    GET_USER_PROFILE_REQUEST: null,
    GET_USER_PROFILE_SUCCESS: null,
    GET_USER_PROFILE_ERROR: null,
  }
);