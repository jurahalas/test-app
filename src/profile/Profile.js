import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Notification from '../components/notification/Notification';
import userAvatar from '../images/userAvatar.jpg';
import CustomButton from '../components/customButton/CustomButton';
import * as profileActions from './ProfileActions';
import mailIcon from '../images/mail.svg';
import './Profile.css';

class Profile extends Component {
  componentDidMount() {
    const { profileActions } = this.props;

    profileActions.getUserProfileRequest();
  }

  handleOpenEditProfile = () => {
    this.props.history.push('/edit-profile');
  };

  render() {
    const { profileData } = this.props;
    const { userData = {} } = profileData;
    const {
      first_name,
      last_name,
      email,
    } = userData;

    return (
      <div className="profile-container">
        <Notification />
        <div className="profile-title-wrapper">
          <div className="profile-title">
            My Profile
          </div>
          <CustomButton
            text='Edit'
            className='profile-edit-btn'
            color='#ffffff'
            clickHandler={ this.handleOpenEditProfile }
          />
        </div>
        <div className="profile-detail">
          <div className="profile-user-image-block">
            <img src={ userAvatar } alt="Avatar" className="profile-image" />
            <div className="profile-btn-wrapper">
              <CustomButton
                text='Change password'
                className='profile-cancel-password'
                color='#005777'
                height={ 30 }
                clickHandler={ this.handleOpenChangePasswordModal }
              />
            </div>
          </div>
          <div className="profile-user-description">
            <div className="profile-user-name">
              { `${first_name} ${last_name}` }
            </div>
            <div className="detailed-description">
              <div className="detailed-item">
                <div className="detailed-title">
                  <div className="detailed-wrapper-icon">
                    <img className="detailed-icon" src={ mailIcon } alt="" />
                  </div>
                  <span className="detailed-title-name">Email</span>
                </div>
                <div className="detailed-value">
                  { email }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  profileActions: PropTypes.object,
  profileData: PropTypes.object,
  history: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);