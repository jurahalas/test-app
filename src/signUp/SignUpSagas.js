import { processRequest } from '../services/Api';
import { takeEvery } from 'redux-saga/effects';
import { put, call, all } from 'redux-saga/effects';
import { signUpActionTypes } from './SignUpConstants';
import * as signUpActions from './SignUpActions';
import { replace } from 'react-router-redux';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeEvery(signUpActionTypes.SIGN_UP_REQUEST, handleRegisterRequest),
  ]);
}

export function* handleRegisterRequest(action) {
  try {
    const { personalId, formData } = action.payload;
    const { email, first_name, last_name, password } = formData;

    const requestPayload = {
      email,
      first_name,
      surname: last_name,
      password,
      personalId,
    };

    const data = yield call(processRequest, 'clients', 'POST', requestPayload);

    if(data.hasOwnProperty('access_token')) {
      yield put(signUpActions.registerSuccess(data));
      yield put(replace('/active-loans'));
    }
  } catch(e) {
    yield put(notificationActions.createNotification('Error during creating user!', true));
    yield put(signUpActions.registerError(e));
  }
}