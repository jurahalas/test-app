import { signUpActionTypes } from './SignUpConstants';

export function acceptTermsToggle(status) {
  return {
    type: signUpActionTypes.SIGN_UP_TERMS_ACCEPT,
    payload: {
      checked: status,
    },
  };
}

export function signUpRequest( personalId, formData) {
  return {
    type: signUpActionTypes.SIGN_UP_REQUEST,
    payload: {
      personalId,
      formData,
    },
  };
}

export function registerSuccess(token) {
  return {
    type: signUpActionTypes.SIGN_UP_SUCCESS,
    payload: {
      token,
    },
  };
}

export function registerError(error) {
  return {
    type: signUpActionTypes.REGISTER_ERROR,
    error: true,
    payload: {
      error,
    },
  };
}

export function switchScreen(screen) {
  return {
    type: signUpActionTypes.SIGN_UP_SWITCH_SCREEN,
    payload: {
      screen,
    },
  };
}

export function clearData() {
  return {
    type: signUpActionTypes.SIGN_UP_CLEAR_DATA,
  };
}

export function saveSignUpData( formData) {
  return {
    type: signUpActionTypes.SAVE_SIGN_UP_DATA,
    payload: {
      formData,
    },
  };
}
