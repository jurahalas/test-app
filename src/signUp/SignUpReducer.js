import { signUpActionTypes } from './SignUpConstants';

const initialState = {
  checked: false,
  formData: {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    confirm_password: '',
  },
  loading: false,
  screen: 'sign-up',
  progressBarInfo: [
    {
      text: 'PERSONAL INFORMATION',
      isActive: false,
      isDone: true,
    },
    {
      text: 'Active Step - PERSONAL ID',
      isActive: true,
      isDone: false,
    },
    {
      text: 'RECEIVE YOUR LOAN',
      isActive: false,
      isDone: false,
    },
  ],
};

export default function signUpReducer(state = initialState, action) {
  const { type, payload = {} } = action;
  const { checked, screen, formData, error, token } = payload;

  switch(type) {

    case signUpActionTypes.SIGN_UP_TERMS_ACCEPT:
      return {
        ...state,
        checked,
      };

    case signUpActionTypes.SAVE_SIGN_UP_DATA:
      return {
        ...state,
        formData,
      };

    case signUpActionTypes.SIGN_UP_SWITCH_SCREEN:
      return {
        ...state,
        screen,
        progressBarInfo: [
          {
            text: `${screen === '' ? 'ACTIVE STEP - ' : '' }PERSONAL INFORMATION`,
            isActive: screen === '',
            isDone: screen !== '',
          },
          {
            text: `${screen === 'add-personal-id' ? 'ACTIVE STEP - ' : '' }PERSONAL ID`,
            isActive: screen === 'add-personal-id',
            isDone: screen !== 'add-personal-id' && screen !== '',
          },
          {
            text: `${screen === 'receive-loan' ? 'ACTIVE STEP - ' : '' }RECEIVE YOUR LOAN`,
            isActive: screen === 'receive-loan',
            isDone: screen !== 'receive-loan' && screen !== 'add-personal-id' && screen !== '',
          },
        ],
      };

    case signUpActionTypes.SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case signUpActionTypes.SIGN_UP_SUCCESS:
      if(action.payload.token.hasOwnProperty('access_token'))
        localStorage.access_token = token.access_token;

      return {
        ...state,
        loading: false,
        token: token.access_token,
      };

    case signUpActionTypes.SIGN_UP_CLEAR_DATA:
      return { ...initialState };

    case signUpActionTypes.REGISTER_ERROR:
      return {
        ...state,
        error,
        loading: false,
      };

    default:
      return state;
  }
}