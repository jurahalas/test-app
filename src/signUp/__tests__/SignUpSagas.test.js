import * as sagas from '../SignUpSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as signUpActions from '../SignUpActions';
import * as notificationActions from '../../components/notification/NotificationActions';
import { replace } from 'react-router-redux';

describe('Sign up success saga tests', () => {
  const action = {
    payload: {
      formData: {
        email: 'test@gmail.com',
        first_name: 'test',
        last_name: 'test',
        password: '123456',
      },
      personalId: '1234',
    },
  };
  const { personalId, formData } = action.payload;
  const { email, first_name, last_name, password } = formData;

  const requestPayload = {
    email,
    first_name,
    surname: last_name,
    password,
    personalId,
  };

  it('Should  successfully register user', () => {
    const generator = sagas.handleRegisterRequest(requestPayload);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'clients', 'POST', requestPayload));

    next = generator.next({ data: { access_token: 'wrdsf' } });
    expect(next.value).toEqual(put(signUpActions.registerSuccess({ data: { access_token: 'wrdsf' } })));

    next = generator.next(true);
    expect(next.value).toEqual(put(replace('/active-loans')));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed when register user', () => {
    const generator = sagas.handleRegisterRequest(requestPayload);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'clients', 'POST', requestPayload));

    next = generator.next();
    expect(next.value).toEqual(put(notificationActions.createNotification('Error during creating user!', true)));

    next = generator.next();
    expect(next.value).toEqual(put(signUpActions.registerError('Error!')));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});
