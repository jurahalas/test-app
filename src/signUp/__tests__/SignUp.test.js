import React from 'react';
import SignUp from '../SignUp';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ store }>
      <SignUp
        router={ {} }
        signUpActions={ {
          acceptTermsToggle: jest.fn(),
          formSubmit: jest.fn(),
        } }
        signUpReducer={
          {
            checked: false,
            formData: {
              first_name: '',
              last_name: '',
              email: '',
              password: '',
              confirm_password: '',
            },
          }
        }
      />
    </Provider>
    , div);
});
