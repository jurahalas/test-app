import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as signUpActions from './SignUpActions';
import './SignUp.css';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import SignUpForm from '../components/forms/signUp/SignUp';
import SignUpAddPersonal from '../components/forms/signUp/SignUpAddPersonal';
import LoanCalculator from '../components/loanCalculator/LoanCalculator';
import Steps from 'react-steps';

class SignUp extends Component {
  componentDidMount() {
    this.props.signUpActions.switchScreen('');
  }

  signUp = (formData) => {
    const { saveSignUpData, switchScreen } = this.props.signUpActions;

    saveSignUpData(formData);
    switchScreen('add-personal-id');
  };

  render() {
    const { signUpReducer, signUpActions, landingPageReducer } = this.props;

    return (
      <BaseWrapper>
        <div className="progress-bar">
          <Steps items={ signUpReducer.progressBarInfo } />
        </div>

        <div className="sign-up-content">
          {
            (signUpReducer.screen === '' || signUpReducer.screen === 'sign-up') &&
            <SignUpForm
              signUpReducer={ signUpReducer }
              signUpActions={ signUpActions }
              onSubmit={ this.signUp }
            />
          }
          {
            signUpReducer.screen === 'add-personal-id' &&
            <SignUpAddPersonal
              signUpReducer={ signUpReducer }
              signUpActions={ signUpActions }
            />
          }
          <div className="sign-up-loan">
            <LoanCalculator
              completeLoan={ true }
              onSubmit={ this.saveData }
              landingPageReducer={ landingPageReducer }
            />
          </div>
        </div>

      </BaseWrapper>
    );
  }
}

SignUp.propTypes = {
  signUpReducer: PropTypes.object.isRequired,
  signUpActions: PropTypes.object.isRequired,
  landingPageReducer: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    signUpReducer: state.signUpReducer,
    landingPageReducer: state.landingPageReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signUpActions: bindActionCreators(signUpActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);