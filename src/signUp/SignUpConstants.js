import keyMirror from 'keymirror';

export const signUpActionTypes = keyMirror(
  {
    SIGN_UP_TERMS_ACCEPT: null,
    SIGN_UP_REQUEST: null,
    SIGN_UP_SUCCESS: null,
    REGISTER_ERROR: null,
    SIGN_UP_SWITCH_SCREEN: null,
    SIGN_UP_CLEAR_DATA: null,
    SAVE_SIGN_UP_DATA: null,
  }
);