import React from 'react';
import HistoryOfLoans from '../HistoryOfLoans';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <HistoryOfLoans />
    </Provider>, document.createElement('div'));
});
