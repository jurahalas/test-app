import React, { Component } from 'react';
import './HistoryOfLoans.css';

class ActiveLoans extends Component {
  render() {
    return (
      <div className="history-of-loans-container">
        <div className="history-of-loans-title-wrapper">
          <div className="history-of-loans-title">
            History of Loans
          </div>
        </div>
      </div>
    );
  }
}

ActiveLoans.propTypes = {

};

export default ActiveLoans;