import { fork } from 'redux-saga/effects';
import signInSagas from './signIn/SignInSagas';
import userProfileSagas from '../src/profile/ProfileSagas';
import forgotPasswordSagas from './forgotPassword/ForgotPasswordSagas';
import signUpSagas from './signUp/SignUpSagas';
import landingPageSagas from './landingPage/LandingPageSagas';

export default function* rootSaga() {
  yield fork(landingPageSagas);
  yield fork(signInSagas);
  yield fork(signUpSagas);
  yield fork(forgotPasswordSagas);
  yield fork(userProfileSagas);
}