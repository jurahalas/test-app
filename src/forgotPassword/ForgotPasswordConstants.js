import keyMirror from 'keymirror';

export const forgotPasswordActionTypes = keyMirror(
  {
    FORGOT_PASSWORD_EMAIL_CHECK: null,
    FORGOT_PASSWORD_REQUEST: null,
    FORGOT_PASSWORD_ERROR: null,
    OPEN_FORGOT_PASSWORD: null,
  }
);