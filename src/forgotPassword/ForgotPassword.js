import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as forgotPasswordActions from '../forgotPassword/ForgotPasswordActions';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import ForgotPasswordForm from '../components/forms/forgotPassword/ForgotPassword';
import ForgotPasswordDone from '../components/forms/forgotPassword/ForgotPasswordDone';

class ForgotPassword extends Component {
  componentDidMount() {
    this.props.forgotPasswordActions.openForgotPassword();
  }

  forgotPassword = (email) => {
    this.props.forgotPasswordActions.forgotPasswordRequest(email);
  };

  render() {
    const { forgotPasswordActions, forgotPasswordReducer } = this.props;

    return (
      <BaseWrapper>
        {
          forgotPasswordReducer.emailIsExist ?
            <ForgotPasswordDone /> :
            <ForgotPasswordForm
              forgotPasswordActions={ forgotPasswordActions }
              onSubmit={ this.forgotPassword }
              forgotPasswordReducer={ forgotPasswordReducer }
            />
        }
      </BaseWrapper>
    );
  }
}

ForgotPassword.propTypes = {
  forgotPasswordActions: PropTypes.object.isRequired,
  forgotPasswordReducer: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    forgotPasswordReducer: state.forgotPasswordReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    forgotPasswordActions: bindActionCreators(forgotPasswordActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);