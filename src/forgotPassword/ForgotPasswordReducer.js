import { forgotPasswordActionTypes } from './ForgotPasswordConstants';

const initialState = {
  emailIsExist: false,
  loading: false,
};

export default function forgotPasswordReducer(state = initialState, action) {
  const { type, payload = {} } = action;
  const { emailStatus, error } = payload;

  switch(type) {

    case forgotPasswordActionTypes.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case forgotPasswordActionTypes.FORGOT_PASSWORD_EMAIL_CHECK:
      return {
        ...state,
        error: '',
        loading: false,
        emailIsExist: emailStatus,
      };

    case forgotPasswordActionTypes.FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        error,
        loading: false,
        emailIsExist: false,
      };

    case forgotPasswordActionTypes.OPEN_FORGOT_PASSWORD:
      return {
        ...state,
        emailIsExist: false,
      };

    default:
      return state;
  }
}