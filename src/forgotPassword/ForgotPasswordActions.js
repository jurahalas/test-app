import { forgotPasswordActionTypes } from './ForgotPasswordConstants';

export function emailCheck(emailStatus) {
  return {
    type: forgotPasswordActionTypes.FORGOT_PASSWORD_EMAIL_CHECK,
    payload: {
      emailStatus,
    },
  };
}

export function forgotPasswordRequest(email) {
  return {
    type: forgotPasswordActionTypes.FORGOT_PASSWORD_REQUEST,
    payload: {
      email,
    },
  };
}

export function forgotPasswordError(error) {
  return {
    type: forgotPasswordActionTypes.FORGOT_PASSWORD_ERROR,
    error: true,
    payload: {
      error,
    },
  };
}

export function openForgotPassword() {
  return {
    type: forgotPasswordActionTypes.OPEN_FORGOT_PASSWORD,
  };
}