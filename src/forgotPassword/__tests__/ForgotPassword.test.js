import React from 'react';
import ForgotPassword from '../ForgotPassword';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <ForgotPassword
        handleSubmit={ jest.fn() }
        ForgotPasswordActions={ {
          acceptTermsToggle: jest.fn(),
          formSubmit: jest.fn(),
        } }
        forgotPasswordReducer={ {
          emailIsExist: false,
        } }
        router={ {} }
      />
    </Provider>, document.createElement('div'));
});
