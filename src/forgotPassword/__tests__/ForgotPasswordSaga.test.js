import * as sagas from '../ForgotPasswordSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as forgotPasswordActions from '../ForgotPasswordActions';
import * as notificationActions from '../../components/notification/NotificationActions';

describe('Forgot password request tests', () => {
  const action = { payload: { email: 'asd@asd.com' } };
  const data = { email: action.payload.email };

  it('Should  successfully done forgot password request saga', () => {
    const generator = sagas.handleForgotPasswordRequest(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'forgot_password', 'POST', data));

    next = generator.next(false);
    expect(next.value).toEqual(put(forgotPasswordActions.emailCheck(false)));

    next = generator.next();
    expect(next.value).toEqual(put(notificationActions.createNotification(false, true)));


    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should fails forgot password request saga in case of an exception', () => {
    const generator = sagas.handleForgotPasswordRequest(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'forgot_password', 'POST', data));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put(notificationActions.createNotification('This email doesn\'t exist in DB', true)));

    next = generator.next();
    expect(next.value).toEqual(put(forgotPasswordActions.forgotPasswordError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

