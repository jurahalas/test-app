import { processRequest } from '../services/Api';
import { takeEvery } from 'redux-saga/effects';
import { put, call, all } from 'redux-saga/effects';
import { forgotPasswordActionTypes } from './ForgotPasswordConstants';
import * as forgotPasswordActions from './ForgotPasswordActions';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeEvery(forgotPasswordActionTypes.FORGOT_PASSWORD_REQUEST, handleForgotPasswordRequest),
  ]);
}

export function* handleForgotPasswordRequest(action) {
  try {
    const requestPayload = {
      email: action.payload.email,
    };

    /*const status = yield call(processRequest, 'forgot_password', 'POST', requestPayload);

    if(status && status.data.length === 0)
      yield put(forgotPasswordActions.emailCheck(true));
    else 
      yield put(forgotPasswordActions.emailCheck(false));*/

    yield put(forgotPasswordActions.emailCheck(true));
  } catch(e) {
    yield put(notificationActions.createNotification('This email doesn\'t exist in DB', true));
    yield put(forgotPasswordActions.forgotPasswordError(e));
  }
}