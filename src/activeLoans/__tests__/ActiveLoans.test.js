import React from 'react';
import ActiveLoans from '../ActiveLoans';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <ActiveLoans />
    </Provider>, document.createElement('div'));
});
