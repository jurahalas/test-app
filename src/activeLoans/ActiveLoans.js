import React, { Component } from 'react';
import './ActiveLoans.css';

class ActiveLoans extends Component {
  render() {
    return (
      <div className="active-loans-container">
        <div className="active-loans-title-wrapper">
          <div className="active-loans-title">
            Active Loans
          </div>
        </div>
      </div>
    );
  }
}

ActiveLoans.propTypes = {

};

export default ActiveLoans;