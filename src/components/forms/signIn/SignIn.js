import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputComponent from '../../inputComponent/InputComponent';
import './SignIn.css';
import { Field, reduxForm } from 'redux-form';
import CustomButton from '../../customButton/CustomButton';
import { Link } from 'react-router-dom';
import email from '../../../images/email.svg';
import password from '../../../images/password.svg';
import { signInValidate } from '../../../services/FormValidate';
import GenericSpinner from '../../spinner/GenericSpinner';
import classNames from 'classnames';

class SignIn extends Component {
  onSubmit = (formData) => {
    this.props.onSubmit(formData.email, formData.password);
  };

  handleKeyDown = (e) => {
    const { invalid, handleSubmit } = this.props;

    if(!invalid && e.keyCode === 13)
      handleSubmit(this.onSubmit)();
  };

  renderField = ({ icon, label, meta: { error, touched, invalid }, input: { onChange, value, name, onBlur, onFocus } }) => {
    const checkType = name === 'password';

    return (
      <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
        <InputComponent
          iconSrc={ icon }
          placeholder={ label }
          onChange={ onChange }
          value={ value }
          onBlur={ onBlur }
          onFocus={ onFocus }
          error={ error && touched }
          onKeyDown={ this.handleKeyDown }
          type={ checkType ? 'password' : 'text' }
        />
        { invalid && error && touched
          ? (
            <div className='error-message-container'>
              <div className="error-message">{ error }</div>
            </div>
          )
          : (
            <div className="helpBlock" />
          )
        }
      </div>
    );
  };

  render() {
    const { handleSubmit, signIn } = this.props;

    return (
      <div className='sign-in-form-wrapper'>
        <div className="sign-in-title">Sign in to your account</div>
        <form className="loginForm" onSubmit={ handleSubmit(this.onSubmit) }>
          <Field
            icon={ email }
            name="email"
            type="email"
            component={ this.renderField }
            label="Enter Email"
          />
          <Field
            icon={ password }
            name="password"
            type="password"
            component={ this.renderField }
            label="Enter Password"
          />
        </form>
        <div className="sign-in-links">
          <div className="forgot-password-link"><Link to='/forgot-password'>Forgot password?</Link></div>
          <div className="signUp-link" onClick={ this.props.signUpActions.clearData }>
            No Account yet? <Link to='/sign-up'>Create one!</Link>
          </div>
        </div>
        <CustomButton
          text={ signIn.loading ? '' : 'Sign In' }
          clickHandler={ handleSubmit(this.onSubmit) }
          className="submit"
          fontFamily="RobotoRegular"
          width={ 400 }
          height={ 50 }
        />
        { signIn.loading &&
        <GenericSpinner
          borderWidth={ 4 }
          height={ 27 }
          width={ 27 }
        />
        }
      </div>
    );
  }
}

SignIn.propTypes = {
  handleSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  signIn: PropTypes.object,
  signUpActions: PropTypes.object,
  invalid: PropTypes.bool,
};

SignIn = reduxForm({
  form: 'SignIn',
  validate: signInValidate,
})(SignIn);

export default SignIn;