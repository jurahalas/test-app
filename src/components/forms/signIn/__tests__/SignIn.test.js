import React from 'react';
import SignIn from '../SignIn';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import Store from '../../../../store/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <SignIn
        SignInActions={ { formSubmit: jest.fn() } }
        handleSubmit={ jest.fn() }
        invalid={ true }
      />
    </Provider>
    , div);
});
