import React from 'react';
import SignUp from '../SignUp';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import Store from '../../../../store/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <SignUp
        signUpActions={ { acceptTermsToggle: jest.fn(), formSubmit: jest.fn() } }
        signUpReducer={
          {
            checked: false,
            formData: {
              first_name: '',
              last_name: '',
              email: '',
              password: '',
              confirm_password: '',
            },
          }
        }
        handleSubmit={ jest.fn() }
        onSubmit={ jest.fn() }
        invalid={ true }
      />
    </Provider>
    , div);
});
