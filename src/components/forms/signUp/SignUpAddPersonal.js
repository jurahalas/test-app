import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputComponent from '../../inputComponent/InputComponent';
import { Field, reduxForm } from 'redux-form';
import CustomButton from '../../customButton/CustomButton';
import personalIdIcon from '../../../images/code.svg';
import { forgotPasswordValidate } from '../../../services/FormValidate';
import classNames from 'classnames';
import GenericSpinner from '../../spinner/GenericSpinner';
import bowser from 'bowser';

class SignUpAddPersonal extends Component {
  onSubmit = (formData) => {
    const { signUpActions, signUpReducer } = this.props;

    signUpActions.signUpRequest(formData.code, signUpReducer.formData);
  };

  handleSubmit = () => {
    const { handleSubmit } = this.props;

    handleSubmit(this.onSubmit)();
  };

  renderField = ({ input, meta: { error, touched, invalid }, icon, label, type, mask }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
      <InputComponent
        iconSrc={ icon }
        placeholder={ label }
        onChange={ input.onChange }
        value={ input.value }
        onBlur={ input.onBlur }
        onFocus={ input.onFocus }
        error={ error && touched }
        mask={ mask }
      />
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  render() {
    const { signUpReducer } = this.props;

    return (
      <div className={classNames('add-personal-form-wrapper', {msie: bowser.msie})}>
        <div className="add-personal-title">Add Personal Info</div>
        <div className='add-personal-description'>
          To verify your person please enter personal id
        </div>
        <form className="addPersonalForm" onSubmit={ this.handleSubmit }>
          <Field
            icon={ personalIdIcon }
            name="code"
            type="number"
            component={ this.renderField }
            label="Enter the code"
            mask="999-999"
          />
          <CustomButton
            text={ signUpReducer.spinnerCheckCode ? '' : 'Sign Up' }
            clickHandler={ this.handleSubmit }
            className="submit"
            fontFamily="Quicksand"
            width={ 400 }
          />
          { signUpReducer.spinnerCheckCode &&
          <GenericSpinner
            borderWidth={ 4 }
            height={ 27 }
            width={ 27 }
            className={
              classNames('sign-up-add-personal-spinner sign-up-add-personal-spinner-default-send-code ',
                {
                  'sign-up-add-personal-spinner-send-code-msi': bowser.msie,
                  'sign-up-add-personal-spinner-send-code-safari': bowser.safari,
                })
            }
            classNameWrapper='sign-up-add-personal-wrapper-spinner'
          />
          }
        </form>
      </div>
    );
  }
}

SignUpAddPersonal.propTypes = {
  signUpActions: PropTypes.object.isRequired,
  signUpReducer: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func,
};

SignUpAddPersonal = reduxForm({
  form: 'AddPersonalInfo',
  validate: forgotPasswordValidate,
})(SignUpAddPersonal);

export default SignUpAddPersonal;