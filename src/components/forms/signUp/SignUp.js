import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputComponent from '../../inputComponent/InputComponent';
import './SignUp.css';
import { Field, reduxForm } from 'redux-form';
import CustomButton from '../../customButton/CustomButton';
import { Link } from 'react-router-dom';
import email from '../../../images/email.svg';
import password from '../../../images/password.svg';
import name from '../../../images/name.svg';
import unchecked from '../../../images/unchecked.svg';
import checked from '../../../images/checked.svg';
import { signUpValidate } from '../../../services/FormValidate';
import RadioButton from '../../radioComponent/RadioButton';
import classNames from 'classnames';
import GenericSpinner from '../../spinner/GenericSpinner';

class SignUp extends Component {
  constructor() {
    super();

    this.state = {
      prevDate: '',
    };
  }

  componentDidMount() {
    this.props.initialize(this.props.signUpReducer.formData);
  }

  onSubmit = (formData) => {
    this.props.onSubmit(formData);
  };

  handleAcceptTerms = () => {
    const { signUpActions, signUpReducer } = this.props;

    signUpActions.acceptTermsToggle(!signUpReducer.checked);
  };

  renderField = ({ icon, label, meta: { error, touched, invalid }, input: { onChange, value, onBlur, onFocus, name } }) => {
    const checkType = name === 'password' || name === 'confirm_password';

    return (
      <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
        <InputComponent
          iconSrc={ icon }
          placeholder={ label }
          onChange={ onChange }
          value={ value }
          onBlur={ onBlur }
          onFocus={ onFocus }
          error={ error && touched }
          type={ checkType ? 'password' : 'text' }
        />
        { invalid && error && touched
          ? (
            <div className='error-message-container'>
              <div className="error-message">{ error }</div>
            </div>
          )
          : (
            <div className="helpBlock" />
          )
        }
      </div>
    );
  };


  render() {
    const { handleSubmit, signUpReducer } = this.props;

    return (
      <div className={ classNames('sign-up-form-wrapper', { invalid: !signUpReducer.checked }) }>
        <div className="sign-up-title">Create your account</div>
        <form className="signUpForm" onSubmit={ handleSubmit(this.onSubmit) }>
          <Field
            icon={ name }
            name="first_name"
            type="text"
            component={ this.renderField }
            label="First Name"
          />
          <Field
            icon={ name }
            name="last_name"
            type="text"
            component={ this.renderField }
            label="Last Name"
          />
          <Field
            icon={ email }
            name="email"
            type="email"
            component={ this.renderField }
            label="Enter Email"
          />
          <Field
            icon={ password }
            name="password"
            type="text"
            component={ this.renderField }
            label="Enter Password"
          />
          <Field
            icon={ password }
            name="confirm_password"
            type="text"
            component={ this.renderField }
            label="Confirm Password"
          />
        </form>
        <div className='sign-up-check-box-container'>
          <RadioButton
            id='accept'
            text='I Accept the Terms of Service'
            className="accept-check-box"
            checked={ signUpReducer.checked }
            selectedIcon={ checked }
            unselectedIcon={ unchecked }
            onChange={ this.handleAcceptTerms }
          />
        </div>
        <CustomButton
          text={ signUpReducer.loading ? '' : 'Next' }
          disabled={ !signUpReducer.checked }
          clickHandler={ handleSubmit(this.onSubmit) }
          className="submit"
          fontFamily="Quicksand"
          width={ 400 }
        />
        { signUpReducer.loading &&
        <GenericSpinner
          borderWidth={ 4 }
          height={ 20 }
          width={ 20 }
          borderColor="rgb(255, 255, 255)"
        />
        }
        <div className='sign-up-link-container'>
          Already have an account? <Link to='/sign-in'>Sign In</Link>
        </div>
      </div>
    );
  }
}

SignUp.propTypes = {
  signUpReducer: PropTypes.object.isRequired,
  signUpActions: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
};

SignUp = reduxForm({
  form: 'SignUp',
  validate: signUpValidate,
})(SignUp);

export default SignUp;
