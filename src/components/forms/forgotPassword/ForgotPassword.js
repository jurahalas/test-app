import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputComponent from '../../inputComponent/InputComponent';
import './ForgotPassword.css';
import { Field, reduxForm } from 'redux-form';
import CustomButton from '../../customButton/CustomButton';
import { withRouter } from 'react-router-dom';
import email from '../../../images/email.svg';
import { forgotPasswordValidate } from '../../../services/FormValidate';
import classNames from 'classnames';
import GenericSpinner from '../../spinner/GenericSpinner';

class ForgotPassword extends Component {
  onSubmit = (formData) => {
    this.props.onSubmit(formData.email);
  };

  onCancel = () => {
    this.props.history.push('/sign-in');
  };

  renderField = ({ icon, label, meta: { error, invalid, touched }, input: { onChange, value, onBlur, onFocus } }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error }) }>
      <InputComponent
        iconSrc={ icon }
        placeholder={ label }
        onChange={ onChange }
        value={ value }
        onBlur={ onBlur }
        onFocus={ onFocus }
        error={ error && touched }
      />
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  render() {
    const { handleSubmit, forgotPasswordReducer } = this.props;

    return (
      <div className="forgot-password-form-wrapper">
        <div className="forgot-password-title">Reset Password</div>
        <div className='forgot-password-description'>
          Enter your email adress and we will send you
          a link to reset your password.
        </div>
        <form className="forgotPasswordForm" onSubmit={ handleSubmit(this.onSubmit) }>
          <Field
            icon={ email }
            name="email"
            type="email"
            component={ this.renderField }
            label="Enter Email"
          />
        </form>
        <div className='forgot-password-buttons-container'>
          <CustomButton
            text='Cancel'
            clickHandler={ this.onCancel }
            className="cancel"
            fontFamily="Quicksand"
            height='50px'
            width='190px'
          />
          <CustomButton
            text={ forgotPasswordReducer.loading ? '' : 'Submit' }
            clickHandler={ handleSubmit(this.onSubmit) }
            className="submit"
            fontFamily="Quicksand"
            height='50px'
            width='190px'
          />
          { forgotPasswordReducer.loading &&
          <GenericSpinner
            borderWidth={ 4 }
            height={ 27 }
            width={ 27 }
            borderColor="rgb(255, 255, 255)"
            className="forgot-pass"
          />
          }
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  history: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  forgotPasswordReducer: PropTypes.object,
  onSubmit: PropTypes.func,
};

ForgotPassword = reduxForm({
  form: 'ForgotPassword',
  validate: forgotPasswordValidate,
})(ForgotPassword);

export default withRouter(ForgotPassword);