import React from 'react';
import ForgotPassword from '../ForgotPassword';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import Store from '../../../../store/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <ForgotPassword />
    </Provider>
    , div);
});
