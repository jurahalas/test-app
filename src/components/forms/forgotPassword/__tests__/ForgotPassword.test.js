import React from 'react';
import ForgotPassword from '../ForgotPassword';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import Store from '../../../../store/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const localStorageMock = {
    getItem: jest.fn(),
    removeItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
  };

  global.localStorage = localStorageMock;
  shallow(
    <Provider store={ Store }>
      <ForgotPassword
        ForgotPasswordActions={ { formSubmit: jest.fn() } }
        handleSubmit={ jest.fn() }
        invalid={ true }
      />
    </Provider>
    , div);
});
