import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CustomButton from '../../customButton/CustomButton';
import { withRouter } from 'react-router-dom';
import mail_in from '../../../images/mail_in.svg';

class ForgotPasswordDone extends Component {
  onSubmit = () => {
    this.props.history.push('/sign-in');
  };

  render() {
    return (
      <div className="forgot-password-done-form-wrapper">
        <div className="forgot-password-done-title">Incoming!</div>
        <div className='forgot-password-done-image'>
          <img src={ mail_in } alt='verified' />
        </div>
        <div className='forgot-password-done-description'>
          We just sent you an email Please follow the link
          in the email to reset your password
        </div>
        <CustomButton
          text='Okay'
          clickHandler={ this.onSubmit }
          className='okay'
          fontFamily='Quicksand'
          height={ 45 }
          width=''

        />
      </div>
    );
  }
}

ForgotPasswordDone.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(ForgotPasswordDone);