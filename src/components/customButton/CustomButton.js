import React from 'react';
import PropTypes from 'prop-types';
import './CustomButton.css';
import classNames from 'classnames';
import SpinnerBtn from '../spinner/SpinnerBtn';

const CustomButton = (props) => {
  const { icon, fontFamily, color, width, height, fontSize, fontWeight, disabled, clickHandler, className, text, spinnerBtn } = props;

  const buttonStyle = {
    width,
    height,
    fontWeight,
    color,
  };

  return (
    <div onClick={ !disabled && clickHandler } className={ `fl-buttonContainer ${className}` } style={ buttonStyle }>
      <div className={ classNames('fl-button', { 'fl-buttonDisabled': disabled }) }>
        <div
          className={ classNames('fl-buttonTextCenter', { 'fl-buttonDisabledOverlay': disabled, 'with-icon': icon }) }
          style={ { fontSize, fontFamily } }
        >
          { icon && <img className="button-icon" src={ icon } alt="icon" /> }
          { spinnerBtn && <SpinnerBtn isShow={ spinnerBtn } /> }
          <div>{ !spinnerBtn && text }</div>
        </div>
      </div>
    </div>
  );
};

CustomButton.defaultProps = {
  fontFamily: '',
  fontSize: '18px',
  width: 190,
  height: 50,
  fontWeight: '300',
  borderRadius: '',
  icon: null,
  disabled: false,
  iconLeft: true,
  color: '#ffffff',
};

CustomButton.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  fontFamily: PropTypes.string,
  fontWeight: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  fontSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  clickHandler: PropTypes.func,
  disabled: PropTypes.bool,
  color: PropTypes.string,
  spinnerBtn: PropTypes.bool,
};

export default CustomButton;
