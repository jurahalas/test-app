import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './RadioButton.css';
import SelectedRadio from '../../images/active.svg';
import UnSelectedRadio from '../../images/inactive.svg';
import iconCheckboxActive from '../../images/check-active.svg';
import iconCheckbox from '../../images/check-inactive.svg';

// TODO: should be refactored

class RadioButton extends Component {
  getIcon = () => {
    const { checked, type, iconRadio, iconRadioChecked, iconCheckbox, iconCheckboxChecked } = this.props;

    if(type === 'radio')
      return checked ? iconRadioChecked : iconRadio;

    return checked ? iconCheckboxChecked : iconCheckbox;
  };

  handleClick = () => {
    const { checked, disabled, onChange } = this.props;

    if(disabled)
      return;

    onChange(!checked);
  };


  render() {
    const { className, classNameLabel, type, id, name, text, isHorizontal, disabled } = this.props;
    const classNamesRadioButton = classNames(`radio-container ${className}`, {
      'radio-button-horizontal': isHorizontal,
      'radio-button-disabled': disabled,
    });

    return (
      <div className={ classNamesRadioButton }>
        <label
          className={`radio-button-label ${classNameLabel}`}
          id={ id }
          title={ text }
          onClick={ this.handleClick }
        >
          <img
            alt="img"
            src={ this.getIcon() }
            className='radio-button-img'
            id={ id }
            type={ type }
            name={ name }
          />
          { text }
        </label>
      </div>
    );
  }
}

RadioButton.defaultProps = {
  className: '',
  classNameLabel: '',
  checked: false,
  type: 'checkbox',
  id: 'checkbox',
  name: 'checkbox',
  text: 'Label',
  isHorizontal: false,
  iconRadio: UnSelectedRadio,
  iconRadioChecked: SelectedRadio,
  iconCheckbox: iconCheckbox,
  iconCheckboxChecked: iconCheckboxActive,
  disabled: false,
  onChange() {
  },
};

RadioButton.propTypes = {
  className: PropTypes.string,
  checked: PropTypes.any,
  type: PropTypes.string,
  id: PropTypes.any.isRequired,
  name: PropTypes.string,
  text: PropTypes.string,
  isHorizontal: PropTypes.bool,
  iconRadio: PropTypes.string,
  iconRadioChecked: PropTypes.string,
  iconCheckbox: PropTypes.string,
  iconCheckboxChecked: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  classNameLabel: PropTypes.string,
};

export default RadioButton;
