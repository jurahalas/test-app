import keyMirror from 'keymirror';

export const notificationActionTypes = keyMirror(
  {
    CREATE_NOTIFICATION_MESSAGE: null,
  }
);
