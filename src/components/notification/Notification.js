import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Notification.css';
import ToastMessage from './toastMessage/NotificationMessage';
import { connect } from 'react-redux';
import classNames from 'classnames';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    const newMessages = this.state.messages;

    newMessages.push({
      message: nextProps.notification.message,
      error: nextProps.notification.error,
      warning: nextProps.notification.warning,
    });
    this.setState({
      messages: newMessages,
    });
  }

  clearMessages = () => {
    this.setState({
      messages: [],
    });
  }

  render() {
    return (
      <div
        className={ classNames('notification-wrapper', { 'notification-wrapper-no-content': this.state.messages.length === 0 }) }
      >
        { this.state.messages.map((el, index) => (
          <ToastMessage
            key={ index }
            message={ el.message }
            error={ el.error }
            warning={ el.warning }
            clearMessages={ this.clearMessages }
          />))
        }
      </div>
    );
  }
}

Notification.propTypes = {
  notification: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    notification: state.notificationReducer,
  };
}

export default connect(mapStateToProps, null)(Notification);
