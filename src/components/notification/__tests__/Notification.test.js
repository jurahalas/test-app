import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Notification from '../Notification';
import Store from '../../../store/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <Provider store={ Store }>
      <Notification
        notification={ {} }
      />
    </Provider>, div);
});
