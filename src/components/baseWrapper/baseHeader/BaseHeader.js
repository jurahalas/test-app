import React from 'react';
import './BaseHeader.css';
import logo from '../../../images/logo.png';
import ButtonsBlock from './buttonsBlock/ButtonsBlock';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

const BaseHeader = ({history}) => {
  const openLandingPage = () => {
    history.push('/');
  };

  return (
    <div>
      <div className="header-container">
        <div className="header-logo">
          <img alt="logo" src={logo} className="site-logo" onClick={ openLandingPage } />
        </div>
        <ButtonsBlock />
      </div>
    </div>
  );
};

BaseHeader.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(BaseHeader);
