import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../../components/customButton/CustomButton';
import { withRouter } from 'react-router-dom';
import './ButtonsBlock.css';

class ButtonsBlock extends Component {
  openProfile = () => {
    this.props.history.push('/sign-in');
  };

  openAboutUs =() => {
    this.props.history.push('/about-us');
  };

  openAchieveGoal =() => {
    this.props.history.push('/achieve-goal');
  };

  render() {
    return (
      <div className="buttons-block-container">
        <CustomButton
          text='How do I request the load'
          clickHandler={ this.openAchieveGoal }
          className='submit'
          fontFamily="Quicksand"
          width={ 240 }
          height={ 30 }
        />
        <CustomButton
          text='About us'
          clickHandler={ this.openAboutUs }
          className='submit'
          fontFamily="Quicksand"
          width={ 150 }
          height={ 30 }
        />
        <CustomButton
          text='Sign In'
          clickHandler={ this.openProfile }
          className='submit'
          fontFamily="Quicksand"
          width={ 150 }
          height={ 30 }
        />
      </div>
    );
  }
}

ButtonsBlock.propTypes = {
  history: PropTypes.object.isRequired,
};

export default (withRouter(ButtonsBlock));