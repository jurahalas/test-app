import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './BaseWrapper.css';
import Notification from '../../components/notification/Notification';
import BaseHeader from './baseHeader/BaseHeader';

class BaseWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      token: '',
      resetModalOpen: false,
    };
  }

  render() {
    const { children } = this.props;

    return (
      <div className="loginization-wrapper-container">
        <BaseHeader />
        <Notification />
        <div className="loginization-container">

          <div className="form-loginization-container">

            { children }

          </div>
        </div>
        <div className="loginization-footer">&copy;{ (new Date().getFullYear()) } 4FINANCE</div>
      </div>
    );
  }
}

BaseWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
  ]),
};

export default BaseWrapper;