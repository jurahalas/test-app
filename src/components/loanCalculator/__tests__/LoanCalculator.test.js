import React from 'react';
import LoanCalculator from '../LoanCalculator';
import { Provider } from 'react-redux';
import store from '../../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <LoanCalculator
        carriersActions={ { 
          getCarrierDetailsRequest: jest.fn(),
          setModal: jest.fn(),
          closeModals: jest.fn(),
          postCarrierAvatarRequest: jest.fn(),
        } }
        landingPageReducer={ {
          loading: false,
          calculateData: {
            amountInterval: {
              min: 5,
              max: 100,
              step: 10,
            },
            termInterval: {
              min: 5,
              max: 100,
              step: 10,
            },
          },
        } }
        onSubmit={ jest.fn() }
        history={ {} }
        completeLoan={ false }
      />
    </Provider>, document.createElement('div'));
});
