import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './LoanCalculator.css';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import NumericInput from 'react-numeric-input';
import Moment from 'moment';
import { DATE_FORMAT_FOR_SHOWING } from '../../services/Constants';
import CustomButton from '../../components/customButton/CustomButton';
import { withRouter } from 'react-router-dom';
import closeIcon from '../../images/close.svg';
import editIcon from '../../images/edit.svg';


class LoanCalculator extends Component {
  constructor() {
    super();
    this.state = {
      amount: 50,
      term: 10,
      interests: 0,
      editLoan: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { landingPageReducer, completeLoan } = nextProps;
    const { calculateData, savedAmount, savedTerm } = landingPageReducer || {};
    const { amountInterval, termInterval } = calculateData || {};

    this.setState({
      amount: savedAmount || amountInterval.defaultValue,
      term: savedTerm || termInterval.defaultValue,
      editLoan: !completeLoan,
    });
  }

  handleChangeAmount = value => {
    this.setState({
      amount: value,
    });
  };

  handleChangeTerm = value => {
    this.setState({
      term: value,
    });
  };

  onChangeAmount = (val) => {
    this.setState({
      amount: val,
    });
  };

  onChangeTerm = (val) => {
    this.setState({
      term: val,
    });
  };

  onSubmit = () => {
    const { amount, term, interests } = this.state;
    const { onSubmit, history } = this.props;

    onSubmit(amount, term, interests);
    history.push('/sign-up');
  };

  handleEditLoan =() => {
    this.setState({
      editLoan: !this.state.editLoan,
    });
  };

  render() {
    const { amount, term, interests, editLoan } = this.state;
    const { landingPageReducer, completeLoan } = this.props;
    const { calculateData } = landingPageReducer || {};
    const { amountInterval, termInterval } = calculateData || {};
    const minAmount = amountInterval.min;
    const maxAmount = amountInterval.max;
    const stepAmount = amountInterval.step;

    const minTerm = termInterval.min;
    const maxTerm = termInterval.max;
    const stepTerm = termInterval.step;

    const amountLabels = {
      100: `${ minAmount }$`,
      1000: `${ maxAmount }$`,
    };

    const termLabels = {
      7: `${ minTerm } days`,
      31: `${ maxTerm }days`,
    };
    const tomorrow = new Date();

    tomorrow.setDate(tomorrow.getDate() + term);

    return (
      <div className="loan-calculator-container">
        <div className="loan-calculator-header">
          <div className="loan-calculator-title">{ completeLoan ? 'Your loan personalized' : 'Calculate your credit' }</div>
          { completeLoan &&

            <CustomButton
              icon={ editLoan ? closeIcon : editIcon }
              text={ editLoan ? 'Save' : 'Edit' }
              clickHandler={ this.handleEditLoan }
              className='edit-btn'
              fontFamily="Quicksand"
              width={ 240 }
              height={ 30 }
            />

          }
        </div>

        { (!completeLoan || editLoan) &&
          <div>
            <div className="loan-section">
              <div className="loan-title">Choose the amount</div>
              <NumericInput
                className='picker-input'
                value={ amount }
                min={ minAmount }
                max={ maxAmount }
                maxLength={ 3 }
                onChange={ this.onChangeAmount }
                pattern='^(0|[1-9][0-9]*)$'
              />
            </div>
            <Slider
              min={ minAmount }
              max={ maxAmount }
              step={ stepAmount }
              value={ amount }
              labels={ amountLabels }
              onChange={ this.handleChangeAmount }
            />
            <div className="loan-section">
              <div className="loan-title">Choose the term</div>
              <NumericInput
                className='picker-input'
                value={ term }
                min={ minTerm }
                max={ maxTerm }
                maxLength={ 2 }
                onChange={ this.onChangeTerm }
                pattern='^(0|[1-9][0-9]*)$'
              />
            </div>
            <Slider
              min={ minTerm }
              max={ maxTerm }
              step={ stepTerm }
              value={ term }
              labels={ termLabels }
              onChange={ this.handleChangeTerm }
            />
          </div>
        }

        <div className="summary-container">
          <div className="item-container">
            <div className="item-title">Amount requested</div>
            <div className="item-value">{ amount }</div>
          </div>
          <div className="item-container">
            <div className="item-title">Interests</div>
            <div className="item-value">{ interests }</div>
          </div>
          <div className="item-container">
            <div className="item-title">Total to return</div>
            <div className="item-value">{ amount }</div>
          </div>
          <div className="item-container">
            <div className="item-title">Return date</div>
            <div
              className="item-value"
            >{ tomorrow !== null ? Moment(tomorrow).format(DATE_FORMAT_FOR_SHOWING) : '' }</div>
          </div>
        </div>
        { !completeLoan &&
        <div className="button-container">
          <CustomButton
            text='Request Your Loan'
            clickHandler={ this.onSubmit }
            className='submit'
            fontFamily="Quicksand"
            width={ 240 }
            height={ 30 }
          />
        </div>
        }
      </div>
    );
  }
}


LoanCalculator.propTypes = {
  landingPageReducer: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  completeLoan: PropTypes.bool.isRequired,
};

LoanCalculator.defaultProps = {
  completeLoan: false,
};


export default withRouter(LoanCalculator);