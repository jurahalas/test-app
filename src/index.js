import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router';
import './index.css';
import Store from './store/Store';
import App from './app/App';
import { appHistory } from './services/HistoryConfig';
import { ConnectedRouter } from 'react-router-redux';
import NotFound from './app/notFound/NotFound';
import SignIn from './signIn/SignIn';
import SignUp from './signUp/SignUp';
import ForgotPassword from './forgotPassword/ForgotPassword';
import Profile from './profile/Profile';
import AboutUs from './aboutUs/AboutUs';
import AchieveGoal from './achieveGoal/AchieveGoal';
import LandingPage from './landingPage/LandingPage';
import ActiveLoans from './activeLoans/ActiveLoans';
import HistoryOfLoans from './historyOfLoans/HistoryOfLoans';


class Main extends Component {
  checkToken = () => (
    localStorage.access_token
  );

  renderStartPage = (props) => (
    this.checkToken(props)
      ? (
        <Redirect to="/active-loans" />
      ) : (
        <LandingPage { ...props } route={ { name: 'LandingPage', value: 'LandingPage' } } />
      )
  );

  renderSignIn = (props) => (
    !this.checkToken(props)
      ? (
        <SignIn { ...props } route={ { name: 'Login', value: 'Login' } } />
      ) : (
        <Redirect to="/" />
      )
  );

  renderSignUp = (props) => (
    !this.checkToken(props)
      ? (
        <SignUp { ...props } route={ { name: 'SignUp', value: 'SignUp' } } />
      )
      : (
        <Redirect to="/" />
      )
  );

  renderForgotPassword = (props) => (
    !this.checkToken(props)
      ? (
        <ForgotPassword { ...props } route={ { name: 'ForgotPassword', value: 'ForgotPassword' } } />
      )
      : (
        <Redirect to="/" />
      )
  );

  renderActiveLoans = (props) => (
    this.checkToken(props)
      ? (
        <App store={ Store } { ...props } route={ { name: 'ActiveLoans', parent: 'ActiveLoans' } }>
          <ActiveLoans
            { ...props }
            route={ {
              name: 'ActiveLoans',
              parent: 'ActiveLoans',
              path: '/active-loans',
            } }
          />
        </App>
      ) : (
        <Redirect to="/" />
      )
  );

  renderHistoryOfLoans = (props) => (
    this.checkToken(props)
      ? (
        <App store={ Store } { ...props } route={ { name: 'HistoryOfLoans', parent: 'HistoryOfLoans' } }>
          <HistoryOfLoans
            { ...props }
            route={ { name: 'HistoryOfLoans', parent: 'HistoryOfLoans', path: '/history-of-loans' } }
          />
        </App>
      ) : (
        <Redirect to="/" />
      )
  );

  renderProfilePage = (props) => (
    this.checkToken(props)
      ? (
        <App store={ Store } { ...props } route={ { name: 'Profile', parent: 'Profile' } }>
          <Profile { ...props } route={ { name: 'Profile', parent: 'Profile' } } />
        </App>
      ) : (
        <Redirect to="/sign-in" />
      )
  );

  renderAboutUs = (props) =>
    !this.checkToken(props)
      ? (
        <AboutUs { ...props } route={ { name: 'AboutUs', value: 'AboutUs' } } />
      ) : (
        <Redirect to="/" />
      );

  renderAchieveGoal = (props) =>
    !this.checkToken(props)
      ? (
        <AchieveGoal { ...props } route={ { name: 'AchieveGoal', value: 'AchieveGoal' } } />
      ) : (
        <Redirect to="/" />
      );

  render() {
    return (
      <Provider store={ Store }>
        <ConnectedRouter history={ appHistory }>
          <Switch>
            <Route
              exact={ true }
              path="/"
              render={ this.renderStartPage }
            />
            <Route
              exact
              name='SignIn'
              path="/sign-in"
              render={ this.renderSignIn }
            />
            <Route
              exact
              name='SignUp'
              path="/sign-up"
              render={ this.renderSignUp }
            />
            <Route
              name='ForgotPassword'
              path="/forgot-password"
              render={ this.renderForgotPassword }
            />
            <Route
              exact={ true }
              path="/active-loans"
              render={ this.renderActiveLoans }
            />
            <Route
              exact={ true }
              path="/history-of-loans"
              render={ this.renderHistoryOfLoans }
            />
            <Route
              exact={ true }
              path="/profile"
              render={ this.renderProfilePage }
            />
            <Route
              exact={ true }
              path="/about-us"
              render={ this.renderAboutUs }
            />
            <Route
              exact={ true }
              path="/achieve-goal"
              render={ this.renderAchieveGoal }
            />
            <Route name="NotFound" component={ NotFound } />
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);