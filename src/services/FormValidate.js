import { EMAIL } from './Constants';

export const signUpValidate = values => {
  const errors = {};

  if(!values.email)
    errors.email = 'Please enter your email';
  else if(!EMAIL.test(values.email))
    errors.email = 'Invalid email address';

  if(!values.first_name)
    errors.first_name = 'Please enter your First Name';

  if(!values.last_name)
    errors.last_name = 'Please enter your Last Name';

  if(!values.password)
    errors.password = 'Please enter your Password';
  if(values.password && values.password.length < 6)
    errors.password = 'Password length should be 6 or more ';

  if(values.password && !values.confirm_password)
    errors.confirm_password = 'Please enter Confirm Password';
  if(values.password && values.confirm_password && values.password !== values.confirm_password)
    errors.confirm_password = 'Passwords are not equal';

  return errors;
};

export const forgotPasswordValidate = values => {
  const errors = {};

  if(!values.email)
    errors.email = 'Please enter your email';
  else if(!EMAIL.test(values.email))
    errors.email = 'Invalid email address';

  return errors;
};

export const signInValidate = values => {
  const errors = {};

  if(!values.email)
    errors.email = 'Please enter your email';
  else if(!EMAIL.test(values.email))
    errors.email = 'Invalid email address';
  if(!values.password)
    errors.password = 'Please enter your password';
  if(values.password && values.password.length < 6)
    errors.password = 'Password length should be 6 or more ';

  return errors;
};

export const profileValidate = values => {
  const errors = {};

  if(!values.first_name)
    errors.first_name = 'Please enter your First Name';

  if(!values.last_name)
    errors.last_name = 'Please enter your Last Name';

  if(!values.username)
    errors.username = 'Please enter your email';
  else if(!EMAIL.test(values.username))
    errors.username = 'Invalid email address';

  return errors;
};