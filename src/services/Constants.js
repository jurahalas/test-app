
export const BASE_API_URL = 'http://localhost:3000/';
export const DATE_FORMAT_FOR_SHOWING = 'MM/DD/YYYY';
export const DATE_FORMAT_FOR_SIGN_IN = 'YYYY-MM-DD HH:mm:ss';
export const EMAIL = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
