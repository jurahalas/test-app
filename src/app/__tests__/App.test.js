import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';
import { Provider } from 'react-redux';
import Store from '../../store/Store';

it('renders without crashing', () => {
  shallow(
    <Provider store={ Store }>
      <App
        route={ { name: 'test' } }
        history={ { push: jest.fn() } }
        store={ {} }
      />
    </Provider>);
});
