import React, { Component } from 'react';
import './MainTemplate.css';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import bowser from 'bowser';
import logo from '../../images/logo.png';
import userAvatar from '../../images/userAvatar.jpg';
import logout from './images/logout.svg';
import CustomButton from '../../components/customButton/CustomButton';
import SideBarItem from './sideBarItem/SideBarItem';
import Notification from '../../components/notification/Notification';
import { withRouter } from 'react-router-dom';
import * as signInActions from '../../signIn/SignInActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileActions from '../../profile/ProfileActions';


class MainTemplate extends Component {
  constructor(props) {
    super();

    this.state = {
      selectedTab: this.getActiveTab(props),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedTab: this.getActiveTab(nextProps),
    });
  }

  getActiveTab = (props) => (
    props.route.parent || ''
  );

  selectTab = () => {
    this.setState({
      selectedTab: this.getActiveTab(this.props),
    });
  };

  openProfile = () => {
    this.props.history.push('/profile');
  };

  logout = () => {
    const { signInActions, history } = this.props;

    signInActions.logout();
    history.push('/');
  };


  render() {
    const { selectedTab } = this.state;

    return (
      <div className='main-template-container'>
        <Notification />
        <div className="template-sideBar-container">
          <div className="template-profile-block">
            <div className="template-logo">
              <img alt="sideBarLogo" src={ logo } />
            </div>
            <div className={ classNames('template-profile-image-container', { 'ie-style': bowser.msie }) }>
              <img
                alt="userAvatar"
                className={ classNames('template-profile-image', { 'ie-style-image': bowser.msie }) }
                src={ userAvatar }
              />
            </div>
            <div className="template-profile-title">{ 'Yuriy' } { 'Halas' }</div>
            <CustomButton
              text='Profile'
              clickHandler={ this.openProfile }
              className={ (selectedTab && selectedTab.indexOf('Profile') !== -1) ? 'submit-active' : 'submit' }
              fontFamily="Quicksand"
              width={ 120 }
              height={ 30 }
            />
          </div>
          <div>
          </div>
          <SideBarItem
            title="ACTIVE LOANS"
            url="/active-loans"
            selectTab={ this.selectTab }
            isSelected={ selectedTab && selectedTab.indexOf('ActiveLoans') !== -1 }
          />
          <SideBarItem
            title="HISTORY OF LOANS"
            url="/history-of-loans"
            selectTab={ this.selectTab }
            isSelected={ selectedTab && selectedTab.indexOf('HistoryOfLoans') !== -1 }
          />
        </div>
        <div className={ 'template-header' }>
          <div className="template-header-container">
            <img className="template-header-icon" onClick={ this.logout } alt="logout" src={ logout } />
          </div>
          <div className='wrapper-container'>
            <div className="template-container">
              { this.props.children }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MainTemplate.propTypes = {
  children: PropTypes.element,
  history: PropTypes.object.isRequired,
  signInActions: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    signInActions: bindActionCreators(signInActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(withRouter(MainTemplate));