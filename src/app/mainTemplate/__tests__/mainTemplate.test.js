import React from 'react';
import MainTemplate from '../MainTemplate';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import Store from '../../../store/Store';

it('renders without crashing', () => {
  shallow(
    <Provider store={ Store }>
      <MainTemplate
        history={ { push: jest.fn() } }
        route={ {} }
      />
    </Provider>);
});