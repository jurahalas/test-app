import React from 'react';
import PropTypes from 'prop-types';
import './SideBarItem.css';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

const SideBarItem = ({ url, image, title, isSelected, selectTab }) => (
  <div
    onClick={ selectTab }
    className={ classnames({
      'sidebar-item': true,
      'selected': isSelected,
    }) }
  >
    <Link
      to={ url }
      className={ classnames({
        'sidebar-item-anchor': true,
      }, { 'side-bar-pointer-event-none': /^\/$/.test(url) }) }
      replace
    >
      { image && <img alt="logo" src={ image } className="imgContainer" /> }
      { title }
    </Link>
  </div>
);

SideBarItem.defaultProps = {
  url: '/',
  isSelected: false,
};

SideBarItem.propTypes = {
  url: PropTypes.string.isRequired,
  image: PropTypes.string,
  title: PropTypes.string.isRequired,
  isSelected: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  selectTab: PropTypes.func,
};

export default SideBarItem;
