import React from 'react';
import { shallow } from 'enzyme';
import SideBarItem from '../SideBarItem';
import Test from './Test.svg';

it('renders without crashing', () => {
  shallow(
    <SideBarItem
      image={ Test }
      title="Test"
      isSelected={ false }
    />);
});
