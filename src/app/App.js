import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import MainTemplate from './mainTemplate/MainTemplate';

class App extends Component {
  render() {
    const { route, history, store } = this.props;

    return (
      <MainTemplate
        route={ route }
        history={ history }
        store={ store }
      >
        { this.props.children }
      </MainTemplate>
    );
  }
}

App.defaultProps = {
  showSideBar: true,
};

App.propTypes = {
  children: PropTypes.element,
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired,
};

export default App;
