import React from 'react';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import './AchieveGoal.css';

const AchieveGoal = () => (
  <BaseWrapper>
    <div className="achieve-goal-page-container">You have 3 simple steps to achieve your goal</div>
  </BaseWrapper>
);

AchieveGoal.propTypes = {
};



export default AchieveGoal;