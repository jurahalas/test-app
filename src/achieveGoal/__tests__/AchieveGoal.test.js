import React from 'react';
import AchieveGoal from '../AchieveGoal';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <AchieveGoal />
    </Provider>, document.createElement('div'));
});
