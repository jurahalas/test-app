import { landingPageActionTypes } from './LandingPageConstants';

const initialState = {
  loading: false,
  calculateData: {
    amountInterval: {
      min: 100,
      max: 1000,
      defaultValue: 100,
      step: 10,
    },
    termInterval: {
      min: 7,
      max: 31,
      defaultValue: 7,
      step: 1,
    },
  },
  savedAmount: null,
  savedTerm: null,
  errors: '',
};

export default function landingPageReducer(state = initialState, action) {
  const { type, payload = {} } = action;
  const { data, errors, amount, term } = payload;

  switch(type) {
    case landingPageActionTypes.GET_CALCULATE_DATA_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case landingPageActionTypes.GET_CALCULATE_DATA_SUCCESS:
      const { amountInterval, termInterval } = data;

      return {
        ...state,
        loading: false,
        calculateData: {
          amountInterval,
          termInterval,
        },
      };

    case landingPageActionTypes.GET_PACKAGES_ERROR:
      return {
        ...state,
        loading: false,
        errors: errors,
      };

    case landingPageActionTypes.SAVE_CALCULATE_DATA:
      return {
        ...state,
        savedAmount: amount,
        savedTerm: term,
      };

    default:
      return state;
  }
}
