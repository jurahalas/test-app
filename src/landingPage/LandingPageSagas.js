import { processRequest } from '../services/Api';
import { takeLatest } from 'redux-saga/effects';
import { put, call, all } from 'redux-saga/effects';
import { landingPageActionTypes } from './LandingPageConstants';
import * as landingPageActions from './LandingPageActions';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeLatest(landingPageActionTypes.GET_CALCULATE_DATA_REQUEST, handleGetCalculateData),
  ]);
}

export function* handleGetCalculateData() {
  try {
    const calculateData = yield call(processRequest, 'application/constraints/', 'GET');

    yield put(landingPageActions.getCalculateDataSuccess(calculateData));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during getting calculate data!', true));
    yield put(landingPageActions.getCalculateDataError(e));
  }
}
