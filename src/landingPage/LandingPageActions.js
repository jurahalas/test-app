import { landingPageActionTypes } from './LandingPageConstants';

export function getCalculateDataRequest() {
  return {
    type: landingPageActionTypes.GET_CALCULATE_DATA_REQUEST,
  };
}

export function getCalculateDataSuccess(calculateData) {
  return {
    type: landingPageActionTypes.GET_CALCULATE_DATA_SUCCESS,
    payload: {
      calculateData,
    },
  };
}

export function getCalculateDataError(errors) {
  return {
    type: landingPageActionTypes.GET_CALCULATE_DATA_ERROR,
    payload: {
      errors,
    },
  };
}
export function saveCalculateData(amount, term, interests) {
  return {
    type: landingPageActionTypes.SAVE_CALCULATE_DATA,
    payload: {
      amount,
      term,
      interests,
    },
  };
}