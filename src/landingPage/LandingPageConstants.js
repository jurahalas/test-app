import keyMirror from 'keymirror';

export const landingPageActionTypes = keyMirror(
  {
    GET_CALCULATE_DATA_REQUEST: null,
    GET_CALCULATE_DATA_SUCCESS: null,
    GET_CALCULATE_DATA_ERROR: null,
    SAVE_CALCULATE_DATA: null,
  }
);