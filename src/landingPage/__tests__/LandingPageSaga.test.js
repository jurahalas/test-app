import * as sagas from '../LandingPageSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as landingPageActions from '../LandingPageActions';
import * as notificationActions from '../../components/notification/NotificationActions';

describe('Landing page request tests', () => {

  it('Should  successfully get calculate data request saga', () => {
    const generator = sagas.handleGetCalculateData();

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'forgot_application/constraints', 'GET'));

    next = generator.next(call(processRequest, 'forgot_application/constraints', 'GET'));
    expect(next.value).toEqual(put(landingPageActions.getCalculateDataSuccess(call(processRequest, 'forgot_application/constraints', 'GET'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should fails get calculate data request saga', () => {
    const generator = sagas.handleGetCalculateData();

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'forgot_application/constraints', 'GET'));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put(notificationActions.createNotification('Error during getting calculate data!', true)));

    next = generator.next();
    expect(next.value).toEqual(put(landingPageActions.getCalculateDataError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

