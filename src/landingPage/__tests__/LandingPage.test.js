import React from 'react';
import LandingPage from '../LandingPage';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <LandingPage
        landingPageActions={ {
          getCalculateDataRequest: jest.fn(),
          saveCalculateData: jest.fn(),

        } }
        landingPageReducer={ {
        } }
      />
    </Provider>, document.createElement('div'));
});
