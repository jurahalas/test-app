import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './LandingPage.css';
import LoanCalculator from '../components/loanCalculator/LoanCalculator';
import * as landingPageActions from './LandingPageActions';

class LandingPage extends Component {
  componentDidMount() {
    this.props.landingPageActions.getCalculateDataRequest();
  }

  saveData = (amount, term, interests) => {
    this.props.landingPageActions.saveCalculateData(amount, term, interests);
  };

  render() {
    const { landingPageReducer } = this.props;

    return (
      <BaseWrapper>
        <div className="landing-page-container">
          <div className="landing-page-section">
            <div className="description">
              <p className="title">Flexible loan</p>
              <p className="sub-title">Adapted to your needs between 1,000 and 10,000 lei</p>
              <p className="section">Convenient </p>
              <p>Apply online and get your money in your account,in less than 24 hours</p>
              <p className="section">Flexible </p>
              <p>You raise the loan amount whenever you want</p>
              <p className="section">Adapted for each </p>
              <p> You decide the monthly rate</p>

            </div>
            <LoanCalculator
              onSubmit={ this.saveData }
              landingPageReducer={ landingPageReducer }
            />
          </div>
        </div>
      </BaseWrapper>
    );
  }
}


LandingPage.propTypes = {
  landingPageActions: PropTypes.object.isRequired,
  landingPageReducer: PropTypes.object.isRequired,

};

function mapStateToProps(state) {
  return {
    landingPageReducer: state.landingPageReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    landingPageActions: bindActionCreators(landingPageActions, dispatch),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);