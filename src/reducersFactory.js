import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import notificationReducer from './components/notification/NotificationReducer';
import signInReducer from './signIn/SignInReducer';
import profileReducer from './profile/ProfileReducer';
import signUpReducer from './signUp/SignUpReducer';
import forgotPasswordReducer from './forgotPassword/ForgotPasswordReducer';

import landingPageReducer from './landingPage/LandingPageReducer';

export default combineReducers({
  routing,
  form: formReducer,
  notificationReducer,
  signInReducer,
  signUpReducer,
  profileReducer,
  forgotPasswordReducer,
  landingPageReducer,
});
