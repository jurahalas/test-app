import React from 'react';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import './AboutUs.css';

const AboutUs = () => (
  <BaseWrapper>
    <div className="about-us-container">About Us</div>
  </BaseWrapper>
);

AboutUs.propTypes = {

};

export default AboutUs;