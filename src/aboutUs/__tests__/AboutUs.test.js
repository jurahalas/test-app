import React from 'react';
import AboutUs from '../AboutUs';
import { Provider } from 'react-redux';
import store from '../../store/Store';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <AboutUs />
    </Provider>, document.createElement('div'));
});
