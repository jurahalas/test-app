import { SignInActionTypes } from './SignInConstants';

const initialState = {
  loading: false,
  error: '',
  token: {},
};

export default function signInReducer(state = initialState, action) {
  const { type, payload = {} } = action;
  const { error, token } = payload;

  switch(type) {

    case SignInActionTypes.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case SignInActionTypes.LOGIN_SUCCESS:
      if(action.payload.token.hasOwnProperty('access_token'))
        localStorage.access_token = token.access_token;

      return {
        ...state,
        error: '',
        loading: false,
        token: token.access_token,
      };

    case SignInActionTypes.LOGOUT:
      localStorage.removeItem('access_token');

      return {
        loading: false,
        error: '',
        token: {},
      };

    case SignInActionTypes.LOGIN_INCORRECT_DATA_PROVIDED:
      return {
        ...state,
        error: '',
        loading: false,
      };

    case SignInActionTypes.LOGIN_ERROR:
      return {
        ...state,
        error,
        loading: false,
      };

    default:
      return state;
  }
}