import { processRequest } from '../services/Api';
import { takeEvery } from 'redux-saga/effects';
import { put, call, all } from 'redux-saga/effects';
import { SignInActionTypes } from './SignInConstants';
import * as signInActions from './SignInActions';
import { replace } from 'react-router-redux';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeEvery(SignInActionTypes.LOGIN_REQUEST, handleLoginRequest),
  ]);
}

export function* handleLoginRequest(action) {
  try {
    const { username, password } = action.payload;
    const requestPayload = {
      username,
      password,
    };

    /*const token = yield call(processRequest, 'authentication', 'POST', requestPayload);

    if(token.data.hasOwnProperty('access_token')) {
      yield put(signInActions.loginSuccess(token));
      yield put(replace('/profile'));
    } else {
      yield put(signInActions.loginIncorrectDataProvided());
      yield put(notificationActions.createNotification('Username or Password is incorrect!', true));
    }*/
    const token = {
      access_token: 'sdfsdfsdgfdgfsfgdfgdsfg',
    };

    yield put(signInActions.loginSuccess(token));
    yield put(replace('/profile'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during sign in!', true));
    yield put(signInActions.loginError(e));
  }
}