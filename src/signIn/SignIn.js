import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SignIn.css';
import BaseWrapper from '../components/baseWrapper/BaseWrapper';
import SignInForm from '../components/forms/signIn/SignIn';
import * as signInActions from './SignInActions';
import * as signUpActions from '../signUp/SignUpActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class SignIn extends Component {
  login = (email, password) => {
    this.props.signInActions.loginRequest(email, password);
  };

  render() {
    const { signIn, signUpActions } = this.props;

    return (
      <BaseWrapper>
        <SignInForm
          signIn={ signIn }
          signInActions={ signInActions }
          onSubmit={ this.login }
          signUpActions={ signUpActions }
        />
      </BaseWrapper>
    );
  }
}


SignIn.propTypes = {
  signIn: PropTypes.object.isRequired,
  signInActions: PropTypes.object.isRequired,
  signUpActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state,
    signIn: state.signInReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signInActions: bindActionCreators(signInActions, dispatch),
    signUpActions: bindActionCreators(signUpActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);